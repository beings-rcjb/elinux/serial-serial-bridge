from __future__ import print_function

from serial import serial_for_url
from timeoutpacketizer import TimeoutPacketizer
import rospy
from std_msgs.msg import UInt8MultiArray
import concurrent.futures as futures
import sys

g_protocol = None


# TODO: Text error handlers


def send_data(packet):
    data = packet.data
    if g_protocol is None:
        sys.stderr.write("[ERROR] send_data: Protocol not yet initialized\n")
    g_protocol.write(bytes(data))


class ReaderDispatcher(TimeoutPacketizer):

    def __init__(self, executor, pub, *args, **kwargs):
        super(ReaderDispatcher, self).__init__(*args, **kwargs)
        self.senderExecutor = executor
        self.pub = pub

    def dispatch(self, publisher, data):
        try:
            publisher.publish(data=list(data))
        except Exception as e:
            print(
                "[ERROR] ReaderDispatcher.dispatch: "
                "Exception thrown in publish:\n{}\n\n"
                .format(e))

    def handle_packet(self, packet):
        try:
            self.senderExecutor.submit(self.dispatch, self.pub, packet)
        except KeyError:
            pass


if __name__ == '__main__':
    from argparse import ArgumentParser
    from serialtrt import ReaderThread
    parser = ArgumentParser(description="")  # TODO: Description
    parser.add_argument('-d', '--serial-device', type=str,
                        required=True,
                        help="Serial device to use")
    parser.add_argument('-b', '--baud', type=int,
                        default=115200,
                        help="Port baud rate")
    parser.add_argument('-t', '--timeout', type=float,
                        default=0.1,
                        help="Protocol frame timeout in seconds")
    parser.add_argument('--frame-id', action='store', type=int, default=3,
                        help="Frame ID to use")
    parser.add_argument('--interface-id', action='store', type=str,
                        default="serial0", help="Interface ID to use")
    parser.add_argument('-A', '--anonymous', action='store_true',
                        help="Initialize ROS node in anonymous mode")

    args = parser.parse_args()

    rospy.init_node("xbee", anonymous=args.anonymous)

    address = args.interface_id + "/" + str(args.frame_id)
    pub = rospy.Publisher(address + "/tx", UInt8MultiArray, queue_size=5)

    with serial_for_url(args.serial_device) as ser:
        ser.baudrate = args.baud
        ser.timeout = args.timeout
        with futures.ThreadPoolExecutor(max_workers=2) as executor:
            with ReaderThread(ser, lambda: ReaderDispatcher(executor, pub))\
                    as protocol:
                g_protocol = protocol

                sub = rospy.Subscriber(address + "/rx", UInt8MultiArray,
                                       send_data)

                rospy.spin()

    g_protocol = None
