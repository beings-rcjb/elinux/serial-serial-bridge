#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

arch=$1

#docker build --pull \
docker build \
	-t serial-serial-bridge:latest-${arch} \
	--build-arg BASE_IMAGE=quay.io/ethanwu10/${arch}-ros \
	.
